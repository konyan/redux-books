export default function () {
    return [
        { title: 'JavaScript Part 1', pages: 101 },
        { title: 'JavaScript Part 2', pages: 102 },
        { title: 'JavaScript Part 3', pages: 103 },
        { title: 'JavaScript Part 4', pages: 104 },
    ];
}